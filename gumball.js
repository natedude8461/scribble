String.prototype.trim = function() {
    return this.replace(/^\s*/,'').replace(/\s*^/, '').replace(/\r\n/,'');
};

Math.randomRange = function(min,max) {
    if (!min) {
        min = 0;
    }   
    if (!max) {
        max = 100;
    }   
    return Math.floor((max - (min - 1))*Math.random()) + min 
};

var net = require('net'),
    sys = require('sys');

var min = 0,
    max = 1000;

var server = net.createServer(function(stream) {

    var randomNumber = Math.randomRange(min,max),
        attempts = 0; 

    stream.setEncoding('utf8');

    stream.on('connect', function() {

        stream.write("\nGumball v0.01\n--------------------------------------------------------------------------------");
        stream.write("\nI have thought of a number between "+min+ " and "+max+" take a guess?");

    });

    stream.on('data', function(data) {
        
        var message = (new String(data)).trim();
        
        var guess = Number(message);

        if (isNaN(guess)) {
            stream.write("\nYou did not enter a number, try again... ");
            return;
        }

        attempts++;

        if (guess < randomNumber) {
            stream.write("\nYour guess is lower than the number I am thinking...");
        } else if (guess > randomNumber) {
            stream.write("\nYour guess is higher than the number I am thinking...");
        } else if (guess == randomNumber) {
            stream.write("\nCongratulations you guessed the random number in " + attempts + " attempts\n");
            stream.end();
        } else {
            stream.write("\nYou should not be seeing this string, if you do then the laws of algebra are broken");
        }


    });

    stream.on('end', function(stream) {

        stream.write('Good Bye!');

    });

}).listen(8000);
